package model;

public class CashCard {
	private String name;
	private String id;
	private double balance;
	private double withdraw;
	private double deposit;
	
	
	public CashCard(String name, String id, double balance){
		this.name = name;
		this.id = id;
		this.balance = balance;
		
	}
	
	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public double getWithdraw() {
		return withdraw;
	}
	
	public void setWithdraw(double withdraw){
		this.withdraw = withdraw;
	}
	
	public double getDeposit() {
		return deposit;
	}

	public void setDeposit(double deposit) {
		this.deposit = deposit;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
	public void deposit(double amount){
		setDeposit(amount);
		balance = balance + amount;
	}
	
	public void withdraw(double amount){
		setWithdraw(amount);
		balance = balance - amount;
	}
	
	
	public String getAmount(double balance){
		String amount = balance + " " ;
		return amount;
	}

	public String toString() {
		return id + "  " + name + " : \n deposit = "+ getAmount(deposit) + "\n withdraw = "
	+ getAmount(withdraw)+"\n Balance = " + balance ;
	}

}

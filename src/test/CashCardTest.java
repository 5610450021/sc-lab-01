package test;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import model.CashCard;
import gui.CashCardFrame;;

public class CashCardTest {
	
	class ListenerMgr implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				System.exit(0);

			}

		}

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			new CashCardTest();
		}

		public CashCardTest() {
			frame = new CashCardFrame();
			frame.pack();
			frame.setVisible(true);
			frame.setSize(400, 400);
			list = new ListenerMgr();
			frame.setListener(list);
			setTestCase();
		}

		public void setTestCase() {
			/*
			 * Student may modify this code and write your result here
			 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
			 */
			CashCard card1 = new CashCard("Thamonwan", "5610450021", 0);
			CashCard card2 = new CashCard("Lalita", "5610450322", 0);
			card1.deposit(500);
			card1.withdraw(64);
			card2.deposit(250);
			card2.withdraw(85);
			frame.setResult(card1.toString());
			frame.extendResult(card2.toString());

		}

		ActionListener list;
		CashCardFrame frame;
	}


